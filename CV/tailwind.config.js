module.exports = {
  content: ["./*.{html,js}"],
  theme: {
    extend: {
      colors: {
        primary: "#110E1A",
        secondary: "#201329",
        footer: "#14101D"
      }
    },
  },
  plugins: [],
}
